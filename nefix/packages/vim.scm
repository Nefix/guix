(define-module (nefix packages vim)
  #:use-module (guix build-system copy)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:))

(define-public vim-spaceduck
  (let ((commit "ffc0065e7b669db8d221e0fa4cfaab72f9423aa6")
	(revision "0"))
    (package
      (name "vim-spaceduck")
      (version (git-version "0.0.0" revision commit))
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
	       (url "https://github.com/pineapplegiant/spaceduck")
	       (commit commit)))
	 (file-name (git-file-name name version))
	 (sha256
	  (base32 "19kp7skydq3ly75pkfjpcwr3vh09w03c05iz4n0g0w9byrm13xd6"))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan
	 '(("autoload" "share/vim/vimfiles/")
	   ("colors" "share/vim/vimfiles/"))))
      (home-page "https://github.com/pineapplegiant/spaceduck/")
      (synopsis "An intergalactic space theme for Vim, Terminal, and more!")
      (description "An intergalactic space theme for Vim, Terminal, and more!")
      (license license:expat))))
