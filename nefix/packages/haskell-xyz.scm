(define-module (nefix packages haskell-xyz)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages haskell-check)
  #:use-module (guix packages)
  #:use-module (guix build-system haskell)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:))

(define-public ghc-data-binary-ieee754
  (package
    (name "ghc-data-binary-ieee754")
    (version "0.4.4")
    (source (origin
	      (method url-fetch)
	      (uri (string-append
		    "https://hackage.haskell.org/package/data-binary-ieee754/"
		    "data-binary-ieee754-" version ".tar.gz"))
	      (sha256
	       (base32
		"02nzg1barhqhpf4x26mpzvk7jd29nali033qy01adjplv2z5m5sr"))))
    (build-system haskell-build-system)
    (home-page "https://john-millikin.com/software/data-binary-ieee754/")
    (synopsis "")
    (description "")
    (license license:gpl3)))


(define-public ghc-hosc
  (package
    (name "ghc-hosc")
    (version "0.18.1")
    (source (origin
	      (method url-fetch)
	      (uri (string-append
		    "https://hackage.haskell.org/package/hosc/"
		    "hosc-" version ".tar.gz"))
	      (sha256
	       (base32
		"0ygyvwzsvqv4pihzdm6i3kzkr01nh3qpk9g9f9ap6243yx7003vj"))))
    (build-system haskell-build-system)
    (inputs
     `(("ghc-blaze-builder" ,ghc-blaze-builder)
       ("ghc-data-binary-ieee754" ,ghc-data-binary-ieee754)
       ("ghc-network" ,ghc-network)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:gpl3)))

(define-public ghc-microspec
  (package
    (name "ghc-microspec")
    (version "0.2.1.3")
    (source (origin
	      (method url-fetch)
	      (uri (string-append
		    "https://hackage.haskell.org/package/microspec/"
		    "microspec-" version ".tar.gz"))
	      (sha256
	       (base32
		"0615gdbsk7i3w71adjp69zabw4mli965wffm2h846hp6pjj31xcb"))))
    (build-system haskell-build-system)
    (native-inputs
     `(("ghc-quickcheck" ,ghc-quickcheck)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:gpl3)))

(define-public ghc-tidal
  (package
    (name "ghc-tidal")
    (version "1.6.1")
    (source (origin
	      (method url-fetch)
	      (uri (string-append
		    "https://hackage.haskell.org/package/tidal/"
		    "tidal-" version ".tar.gz"))
	      (sha256
	       (base32
		"13n9s0s04bddl16xq86anz7a9fqcm7j3xfqn5y1mni5j1h7hn2k2"))))
    (build-system haskell-build-system)
    (inputs
     `(("ghc-bifunctiors" ,ghc-bifunctors)
       ("ghc-clock" ,ghc-clock)
       ("ghc-colour" ,ghc-colour)
       ("ghc-hosc" ,ghc-hosc)
       ("ghc-network" ,ghc-network)
       ("ghc-primitive" ,ghc-primitive)
       ("ghc-random" ,ghc-random)
       ("ghc-vector" ,ghc-vector)))
    (native-inputs
     `(("ghc-microspec" ,ghc-microspec)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:gpl3)))
