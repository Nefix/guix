(define-module (nefix packages emacs-xyz)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (guix packages)
  #:use-module (guix build-system emacs)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:))

(define-public emacs-tidal
  (package
    (name "emacs-tidal")
    (version "1.6.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://raw.githubusercontent.com/tidalcycles/Tidal/" version "/tidal.el"))
       (sha256
	(base32 "0a6grqlkg26c9lvr9jbkvi1wwrjfpks8y78gvj2v4abi6yblnbd9"))))
    (propagated-inputs
     `(("emacs-haskell-mode" ,emacs-haskell-mode)))
    (build-system emacs-build-system)
    (home-page "https://tidalcycles.org")
    (synopsis "")
    (description "")
    (license license:expat)))

(define-public emacs-ayu-theme
  (let ((commit "ed98a9f41d9f0e08458ee71cc1038f66c50e1979")
	(revision "0"))
    (package
      (name "emacs-ayu-theme")
      (version (git-version "20200521" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
	       (url "https://github.com/vutran1710/Ayu-Theme-Emacs")
	       (commit commit)))
         (sha256
	  (base32 "1qdw9pq1daydky0b94x248q27sjzaxpfw9d027xk6ygi9hksvcsk"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/vutran1710/Ayu-Theme-Emacs")
      (synopsis "Ayu theme for Emacs")
      (description "Ayu theme for Emacs")
      (license license:expat))))
