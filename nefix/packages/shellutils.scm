(define-module (nefix packages shellutils)
  #:use-module (guix build-system copy)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:))

(define-public oh-my-zsh
  (let ((commit "86f805280f6a8cf65d8d0a9380489aae4b72f767")
	(revision "0"))
    (package
      (name "oh-my-zsh")
      (version (git-version "0.0.0" revision commit))
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
	       (url "https://github.com/ohmyzsh/ohmyzsh/")
	       (commit commit)))
	 (file-name (git-file-name name version))
	 (sha256
	  (base32 "1wf4g1z7fvravsp020xdqvczf4kcw1nh3b22djlsgd97n8qgziaz"))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan
	 '(("lib" "lib")
	   ("plugins" "plugins")
	   ("themes" "themes")
	   ("oh-my-zsh.sh" "oh-my-zsh.sh"))))
      (home-page "https://ohmyz.sh")
      (synopsis "Oh My Zsh is an open source, community-driven framework for managing your zsh configuration.")
      (description "A delightful community-driven (with 1700+ contributors) framework for managing your zsh configuration. Includes nearly 300 optional plugins (rails, git, OSX, hub, capistrano, brew, ant, php, python, etc), over 140 themes to spice up your morning, and an auto-update tool so that makes it easy to keep up with the latest updates from the community.")
      (license license:expat))))
