(define-module (nefix machines cleopatra)
  #:use-module (gnu)
  #:use-module (gnu services base)
  #:use-module (gnu services networking)
  #:use-module (gnu services ssh)
  #:use-module (guix channels)
  #:use-module (nefix services ci))

(define-public cleopatra
  (operating-system
    (locale "ca_ES.utf8")
    (timezone "Europe/Madrid")
    (keyboard-layout
     (keyboard-layout "us" "altgr-intl"))
    (host-name "cleopatra")
    (users (cons* (user-account
                   (name "nefix")
                   (comment "Néfix Estrada")
                   (group "users")
                   (home-directory "/home/nefix")
                   (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                  %base-user-accounts))
    (sudoers-file
     (plain-file "sudoers"
                 (string-append (plain-file-content %sudoers-specification)
	                        (format #f "~a ALL = NOPASSWD: ALL~%"
		                        "nefix"))))
    (packages
     (append
      (list (specification->package "nss-certs"))
      %base-packages))
    (services
     (append
      (list (service openssh-service-type
		     (openssh-configuration
		      (permit-root-login #f)
		      (password-authentication? #f)
		      (authorized-keys
		       `(("nefix" ,(local-file "../../keys/nefix.pub"))))))
            (service network-manager-service-type)
            (service wpa-supplicant-service-type)
	    (service guix-publish-service-type
                     (guix-publish-configuration
                      (host "0.0.0.0")
                      (port 8080)
                      (compression '(("lzip" 7) ("gzip" 9)))
                      (cache "/var/cache/guix/publish")
                      ;; 1 month
                      (ttl 2592000)))
	    (service channel-ci-service-type))
      (modify-services %base-services
	(guix-service-type config =>
			   (guix-configuration
			    (inherit config)
			    (authorized-keys
			     (append (list (local-file "/etc/guix/signing-key.pub"))
				     %default-authorized-guix-keys)))))))
    (bootloader
     (bootloader-configuration
      (bootloader grub-bootloader)
      (target "/dev/sda")
      (keyboard-layout keyboard-layout)))
    (swap-devices
     (list (uuid "2e8f73ef-99e4-4b44-9c7c-209b73ed8dc2")))
    (file-systems
     (cons* (file-system
              (mount-point "/")
              (device
               (uuid "512ff610-2ed3-4ced-866a-7006e354b3b5"
                     'ext4))
              (type "ext4"))
            %base-file-systems))))

