(define-module (nefix services ci)
  #:use-module (ice-9 match)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix modules)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (gnu system shadow)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages package-management)
  #:export (channel-ci-configuration))

;; Stolen and adapted from https://github.com/leibniz-psychology/psychnotebook-deploy/blob/master/src/zpid/machines/yamunanagar/ci.scm

(define-record-type* <channel-ci-configuration>
  channel-ci-configuration make-channel-ci-configuration
  channel-ci-configuration?
  (channels channel-ci-configuration-channels
	    (default '()))
  (package-modules channel-ci-configuration-packages
		   (default '())))

;; TODO: Make this customizable via settings
(define (make-channels channels)
  (scheme-file "channels-for-channels-ci.scm"
	       #~(cons* (channel
                         (name 'nonguix)
                         (url "https://gitlab.com/nonguix/nonguix")
                         ;; Enable signature verification:
                         (introduction
                          (make-channel-introduction
                           "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
                           (openpgp-fingerprint
                            "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
			(channel
			 (name 'nefix)
			 (url "https://gitlab.com/nefix/guix"))
                        %default-channels)))

;; TODO: Make this customizable via settings
(define (make-manifest packages)
  (scheme-file "manifest-for-channels-ci.scm"
	       #~(begin
		   (use-modules
		    (guix packages)
		    (srfi srfi-1))

		   (define not-package?
		     (lambda (p) (not (package? p))))

		   (define module->packages
		     (lambda (mod)
		       (remove not-package?
			       (hash-map->list (lambda (name var) (variable-ref var))
					       (module-obarray (resolve-interface mod))))))

		   (define manifest-packages
		     (map module->packages
			  (list '(nefix packages telegram)
				'(nefix packages haskell-xyz)
				'(nefix packages emacs-xyz)
				'(nefix packages vim)
				'(nongnu packages mozilla))))

		   (packages->manifest (apply append manifest-packages)))))


;; TODO: Be able to also compile system configurations
(define (runner channels packages)
  (let* ((code (with-imported-modules
		   (source-module-closure '((guix build utils)))
		 #~(begin
		     (use-modules (guix build utils))

                     (define (loop)
                       (invoke #$(file-append guix "/bin/guix")
	                       "time-machine"  "-C" #$(make-channels channels)
	                       "--" "build" "--timeout=14400" "--keep-going" "-m" #$(make-manifest packages)
	                       "--no-grafts")
                       (sleep 60)
                       (loop))

		     (loop)))))
    (program-file "channel-ci-runner" code)))

(define channel-ci-shepherd-service
  (match-lambda
    (($ <channel-ci-configuration> channels package-modules)
     (list (shepherd-service
	    (provision '(channel-ci))
	    (documentation "Run the guix-ci daemon.")
	    (requirement '(user-processes networking))
	    (start #~(make-forkexec-constructor
		      '(#$(runner channels package-modules))
		      #:user "channelbuilder"
		      #:group "channelbuilder"
		      #:log-file "/var/log/channel-ci.log"))
	    (stop #~(make-kill-destructor)))))))

(define %channel-ci-accounts
  (list (user-group
         (name "channelbuilder")
         (system? #t))
        (user-account
         (name "channelbuilder")
         (group "channelbuilder")
         (system? #t)
         (comment "channel-ci-service Build User")
         (home-directory "/var/cache/channelbuilder")
         (shell (file-append shadow "/sbin/nologin")))))

(define-public channel-ci-service-type
  (service-type
   (name 'channel-ci-service-type)
   (extensions
    (list (service-extension shepherd-root-service-type
			     channel-ci-shepherd-service)
          (service-extension account-service-type
			     (const %channel-ci-accounts))))
   (description "Simple channel CI service")
   (default-value (channel-ci-configuration))))
